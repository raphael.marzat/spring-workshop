# Mise en place Spring / Spring Boot

Rendez vous sur le site de [Spring Initializr](https://start.spring.io/) et générez un projet avec la configuration suivante :

- Project: Maven
- Language: Java
- Spring Boot: 2.7.17
- Project Metadata
  - Group: fr.univ.larochelle
  - Artifact: demo-helloworld
  - Name: demo-helloworld
  - Description: Hello World Demo project
- Packaging : Jar
- Java : 17

Ouvrez le projet à l'aide d'IntelliJ (qui supporte nativement les projets Maven / Spring), ou installez l'extension "Spring Boot Extension Pack" si vous travaillez avec VSCode. 

## Fichiers générés par Spring Initializr

Spring Initializr est un outil permettant de configurer et créer des projets Spring Boot facilement, en y ajoutant les dépendances nécessaires à l'aide d'une interface graphique simple et intuitive. 

Cet outil génère un certains nombre de fichiers intéressant décrits ci-dessous :

**pom.xml, .mvn, mvnw & mvnw.cmd**
Ces fichiers sont directement liés au gestionnaire de source que l’on choisit à l'initialisation du projet. Ils permettent l’exécution de maven sans installation.
Dans le cadre d’un projet gradle, les fichiers build.*gradle, gradlew, gradlew.bat, settings.gradle* et le répertoire gradle sont générés.

Pour utiliser le wrapper maven il faut exécuter le script

```bash
./mvnw clean install
```

**src/main/java**
Ce dossier contient les packages et les classes Java nécessaire à l'exécution de l'application. 

**src/test/java**
Ce dossier contient l'ensemble des tests de l'application. On lui donne souvent la même structure interne que le package ``src/main/java``.

**src/main/resources & src/test/resources**
Ici, nous retrouverons l’ensemble des ressources de notre application, il pourra s’agir de fichiers de propriétés (.properties ou .yml), de fichiers de templating (thymeleaf, freemarker), fichiers de configuration (.xml) où encore de fichiers statiques HTML, JSON, par exemple.

**DemoHelloworldApplication.java**
Ce fichier est la classe principale de notre application, la méthode ``main(String[] args)`` indique qu’il s'agit du point d’entrée de notre application.
On constate aussi que la classe est annotée @SpringBootApplication (on y reviendra plus tard), cette annotation indique à Spring Boot qu’il faut, au lancement de l’application, préparer le Spring Context et l’ensemble des composants associés.

## Lancement du projet

Ajoutez la méthode suivante dans le fichier ``DemoHelloWorldApplication``. Elle permet d'exécuter du code directement après le lancement de l'application.

```java
@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		System.out.println("Hello World!");
	}
```

Lancez ensuite le projet et vérifiez que tout fonctionne. 

## Utilisations du Spring Container et des beans 

### Mise en place 

Dans un premier temps, nous allons créer quelques classes en utilisant uniquement du Java classique. 

Créez une classe ``ConsolePrinter`` qui contient une méthode ``void print(String str)`` et qui écrit le message passé en paramètre dans la console.  

Utilisez cette classe dans la méthode ``printHelloWorld`` d'une classe ``HelloWorldService`` pour écrire le message "Hello World!".


Créez une nouvelle classe ``FilePrinter``. Cette classe permet d'écrire la chaine de la méthode print() dans un fichier. 

```java
public class FilePrinter  {

    /** Constructor... **/

    public void print(String str) {
        try {
            FileWriter fileWriter = new FileWriter(filename);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.print(str);
            printWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
```

Nous pouvons utiliser cette nouvelle classe dans ``HelloWorldService``. Cependant, il serait plutôt souhaitable que HelloWorldService n'ait pas à se soucier des détails d'implémentation tels que le type de ``Printer`` à utiliser. 

Pour ce faire, nous pouvons utiliser l'inversion de dépendance.

Créez une interface ``Printer``, qui sera implémentée par ``ConsolePrinter``. ``HelloWorldService`` peut maintenant prendre en paramètre un ``Printer`` de type inconnu, et ce sera au moment de son initialisation que l'on définira quel type de ``Printer`` utiliser.  

Cependant, celà veut dire que l'on doit définir les dépendances dans la classe qui initialise ``HelloWorldService``. 

Pour le moment, on obtient le résultat suivante : 

```java
HelloWorldService helloWorldService = new HelloWorldService(new ConsolePrinter());
```

C'est toujours lisible, mais si le nombre de dépendances augmente, cela pourrait devenir difficile à lire. 

C'est ici que Spring et son Spring Container entrent en jeu, car ils permettent de gérer facilement les dépendances entre les différentes classes.

### Utilisation des Beans 

Pour le moment, nous avons instancié et utlisé les classes manuellement. Nous allons maintenant utiliser Spring pour gérer les dépendances entre nos classes.  

Créez un bean pour les classes ``HelloWorldService`` et ``ConsolePrinter``. 
Vous pouvez vous baser sur [ce tutoriel](https://www.geeksforgeeks.org/how-to-create-a-spring-bean-in-3-different-ways/) pour voir les différentes manières de créer un Bean. 

Injectez le Bean de ``ConsolePrinter`` dans ``HelloWorldService``, et vérifiez que le Printer utilisé est bien le bon.

### Gestion de Beans multiples

Créez un bean pour la classe ``FilePrinter``. Pour déclarer un Bean lié à cette classe, il sera préférable de passer par une classe annotée ``@Configuration`` et d'utiliser une méthode annotée ``@Bean`` afin de pouvoir définir le nom du fichier dans lequel sauvegarder le contenu. 

Si vous essayez de lancer le programme, vous devriez avoir une erreur. En effet, 2 Beans de type ``Printer`` sont déclarés, et Spring ne sait pas lequel injecter dans la classe ``HelloWorldService``.

Utilisez l'annotation ```@Qualifier`` pour lui permettre de choisir un des deux beans. 

### Utilisation de la configuration

Nous souhaitons faire en sorte que le nom du fichier à l'intérieur duquel le contenu sera écrit par ``FileWriter`` ne soit pas mis en dur dans le code mais défini dans le fichier application.yml.

> Vous pouvez renommer application.properties en application.yml pour pouvoir utiliser la syntaxe YML.

Dans le fichier application.yml, définissez la propriété suivante : 

```yml
log:
  filename: example.txt
```

Utilisez l'annotation ``@Value`` afin d'accéder au contenu du fichier application.yml pour créer le Bean `filePrinter` qui écrira dans le fichier défini dans la configuration. 

### Déclaration de Bean conditionnelle

Avec la configuration actuelle, si la propriété `log.filename` n'est pas définie, le programme ne se lancera pas. 

Nous souhaitons désormais faire en sorte que si cette propriété est définie, le bean ``filePrinter`` soit déclaré, et dans le cas contraire, ce soit le bean ``consolePrinter`` qui soit déclaré. 

Mettez en place ce mécanisme en vous basant sur les annotation 
``@Conditional*``.